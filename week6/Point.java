package week6;

public class Point {

    int x;
    int y;

    public Point(){
        System.out.println("Point is created");
    }
    public Point(int x, int y){
        System.out.println("Point is created with x = " +
                x + " y = " + y);
        this.x = x;
        this.y = y;
    }

    public void move(int xDist, int yDist){
        x+= xDist;
        y+= yDist;
    }

    public double distanceFromOrigin(){
        return Math.sqrt(x*x +y*y);
    }


    public double distanceFromAPoint(Point p){
        return Math.sqrt((x - p.x) *(x - p.x)
                +(y - p.y)*(y - p.y));
    }


    public static void main(String[] args) {
        String str = "hello";
        str.length();
        Point p = new Point(4,5);
        p.move(2,2);
        System.out.println("p.x = " + p.x);
        System.out.println("p.x = " + p.y);

        Point p2 = new Point();
        p2.move(8, 11);
//        p2.x = 6;
//        p2.y = 8;
        System.out.println("p2.x = " + p2.x);
        System.out.println("p2.y = " + p2.y);

        System.out.println("p2 distance from Origin = " + p2.distanceFromOrigin());
        System.out.println("p2 distance from p = " + p2.distanceFromAPoint(p));
    }
}
