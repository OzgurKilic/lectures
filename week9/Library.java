package week9;

public class Library {

    int size = 10;
    Book[] contents = new Book[size];

    int count = 0;

    public void addBook(Book book){
        if (count < size) {
            System.out.println("adding book " + book.name);
            contents[count] = book;
            count++;
        }else{
            Book[] newContents = new Book[size*2];
            for(int i=0; i<size; i++){
                newContents[i] = contents[i];
            }
            newContents[size] = book;
            contents = newContents;
            size = size*2;
            count++;

            System.out.println("Library expanded. And added " + book.name);
        }
    }

    public void printContents(){
        for (int i = 0; i < count; i++){
            System.out.println(contents[i].name);
        }
    }

}
