package week9;

//import week10.Rectangle;

public class TestLibrary {

    public static void main(String[] args){

        //Rectangle r = new Rectangle(4,5);
        //r.length = 5;
        //r.area();

        LibraryV2 lib = new LibraryV2();

        lib.addBook(new Book("1984"));
        lib.addBook(new Book("Butterfly"));

        for(int i = 0; i<9; i++){
            lib.addBook(new Book(i + ""));
        }


        lib.printContents();
    }
}
