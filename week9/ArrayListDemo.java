package week9;

import java.util.ArrayList;

public class ArrayListDemo {

    public static void main(String[] args){
        ArrayList<String> names = new ArrayList<>();

        names.add("Ali");
        names.add("Mugla");
        //names.add(5);   error
        //names.add(new Book("Istanbul")); //error

        double[] arr = new double[10];
        ArrayList<Double> values = new ArrayList<Double>();

        values.add(5.5);
        values.add(3.4);

        ArrayList<Integer> intValues = new ArrayList<Integer>();
        intValues.add(6);
        //intValues.add(5.5);

    }
}
