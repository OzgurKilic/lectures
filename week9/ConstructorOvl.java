package week9;

public class ConstructorOvl {
    int a = 0;
    int b = 0;
    int c = 0;



    public ConstructorOvl(int a, int b, int c) {
        this.a= a;
        this.b = b;
        this.c = c;
    }


    public ConstructorOvl(int a) {
        this.a= a;

    }

    public ConstructorOvl(ConstructorOvl obj1) {

        a = obj1.a;
        b= obj1.b;
        c= obj1.c;
    }


    public static void main(String[] args){
       ConstructorOvl obj1 = new ConstructorOvl(3,4,5);

       ConstructorOvl obj2 = new ConstructorOvl(3,0,0);

       obj2 = new ConstructorOvl(3);

       obj2 = new ConstructorOvl(obj1.a, obj1.b, obj1.c);

       obj2 = new ConstructorOvl(obj1);

    }

}
