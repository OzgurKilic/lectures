package week9;

import java.util.ArrayList;

public class LibraryV2 {


    ArrayList<Book> contents = new ArrayList<Book>();

    public void addBook(Book book){
        System.out.println("adding book " + book.name);
        contents.add(book);

    }

    public void printContents(){
        for (int i = 0; i < contents.size(); i++){
            System.out.println(contents.get(i).name);
        }
    }

}
