package week14;

import week10.Square;

import java.util.ArrayList;

public class GenericsArrayList {

    public static void main (String[] args){
        ArrayList<Integer> lst = new ArrayList<>();
        //lst.add("Hello");
        lst.add(5);

        Integer str = lst.get(0);
        System.out.println(str);


        Box<String> box = new Box("Hello");
        box.setObj("Merhaba");

        String strBox = box.getObj();
        System.out.println(strBox);

        Pair<String, String> pairStr = new Pair<>("key", "value");
        Pair<Integer, String> pairIntStr = new Pair<>(5,"Hello");
        Pair<Integer, Box<Integer>> pairIntBox = new Pair<>(4, new Box<Integer>(4));

    }
}
