package week14;

public class TestException {

    public static void main(String[] args) throws  Exception{
        System.out.println("A");


        try {
            foo(5);
        }catch (Exception ex){
            System.out.println("recover");
        }
        finally{
            System.out.println("give resources back");
        }

        System.out.println("I");

        //After recovery flow continues normally
        System.out.println("End of main");
    }

    private static void foo(int i) throws Exception {

        System.out.println("B");
        if (i == 5) {
            try {
                arithmo(i);
            }catch (Exception ex){
                System.out.println("partial recover");
                throw ex;
            }

        } else if (i == 6) {
            System.out.println("E");
            String s = null;
            s.length();
            System.out.println("F");
        }


        System.out.println("G");
    }

    private static void arithmo(int i) throws Exception {

            System.out.println("C");
           if (i == 5){
               throw new Exception();
           }
            System.out.println("D");

    }


}
