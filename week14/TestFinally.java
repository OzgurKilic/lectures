package week14;

public class TestFinally {

    public static void main(String[] args){
        foo();
        System.out.println("E");
    }

    private static void foo() {
        System.out.println("A");
        int i = 1;
        if (i == 1)
            throw new ArithmeticException();
        try{
            System.out.println("B");
            if (i== 1)
                return;
        }finally {
            System.out.println("C");
        }
        System.out.println("D");
    }
}
