package quiz1;

public class Question4 {

    public static void main(String[] args) {

        System.out.println(findMin(4,6,3,7));
    }

    public static int findMin(int a, int b, int c, int d){
        int min1 = a < b ? a : b;
        int min2 = c < d ? c : d;

        return  min1 < min2 ? min1 : min2;

    }

    public static int findMinV1(int a, int b, int c, int d){
        if(a <= b && a<=c && a<=d)
            return a;
        if(b <= a && b<=c && b<=d)
            return b;
        if(c <= a && c<=b && c<=d)
            return c;

        return d;


    }
}
