package quiz1;

public class Question3 {

    public static void main(String[] args) {
        int a = 10;
        int b = a++;
        boolean bool = (b==10) || (7 == ++a);

        int c = bool ? --b : a++;

        int d = a++ + --b + c++;

        System.out.println("a="+a+" b="+b+" c="+c+" d="+d);
    }
}
