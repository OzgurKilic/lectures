package week3;

public class PrecedenceDemo {

    public static void main(String[] args) {
        int i = 2;
        boolean bool = (i < 5) || (i < 3) && (++i < 3);

        System.out.println( "bool :" + bool +" i: " + i);

        bool = (i > 5) && (i >3) || (i++ < 3);
        System.out.println( "bool :" + bool +" i: " + i);

        int a = 0;
        System.out.println(++a + ++a * ++a);

        a = 0;
        System.out.println(a++ + a++ * a++ + " a :" + a);
        System.out.println(a);

    }
}
