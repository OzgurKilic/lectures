package week3;

public class SqrtDemo {

    public static void main(String[] args) {
        double num , sroot;

        for (num = 1; num < 100; num++){
            sroot = Math.sqrt(num);
            System.out.println("Square Root "
            + " of " + num + " is "
            + sroot);
        }
    }
}
