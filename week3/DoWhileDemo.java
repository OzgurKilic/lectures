package week3;

public class DoWhileDemo {

    public static void main(String[] args) {
        int i = 0;

        do{
            System.out.println("Hello Do While");
        }while (i < 0);


        i = 0;

        while(i < 0){
            System.out.println("Hello While");
        }
    }
}
