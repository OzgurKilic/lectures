package week3;

public class UnaryDemo {


    public static void main(String[] args) {
       int a = 5;
       int b = a;
       int c = -b;

       b = a--;
        System.out.println("b = a--  : " + b);
        System.out.println("a  : " + a);
        System.out.println("b  : " + b);

        boolean bool = a == b;

        System.out.println("a == b : " + bool);

        System.out.println("!(a == b) : " + !(a == b));

    }
}
