package week4;

public class LabeledBreak {

    public static void main(String[] args) {
        outer:for (int i = 0; i < 3; i++) {
            for (int j = 2; j < 5; j++) {
                if (j == 4)
                    break outer;
                System.out.println(i + "," + j);
            }

        }

        for (int i = 0; i < 3; i++)
            outer:{
            for (int j = 2; j < 6; j++) {
                if (j == 4)
                    break outer;
                System.out.println(i + "," + j);
            }

        }

    }
}
