package week4;

public class LoopContinue {
    public static void main(String[] args) {

        for (int i = 0; i < 100; i++) {
            if (i >= 50  && i<70)
                continue;
            System.out.println(i);
        }
    }
}
