package week4;

public class ForVar {

    public static void main(String[] args) {
        int sum = 0;
        int fact = 1;

        int i=1;
        for(; i<=5; i++){
            sum += i; // sum = sum +i;
            fact *=i; //fact = fact * i
        }
        System.out.println("i = " +i); //Error

        System.out.println("sum = " + sum +", fact = " + fact);
    }
}
