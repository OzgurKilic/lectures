package week4;

public class ReturnDemo {

    public static void main(String[] args) {
         /*int min = findMin(2,3);
        System.out.println("min = " + min);*/
         doSomething();
    }

    private static int findMin(int a, int b) {
        if (a < b) {
            return a;

        }
        System.out.println("If is FALSE");
        return b;
    }

    private static void doSomething(){
        int x =0;
        while (x<10){
            if (x == 5)
                return;
            System.out.println(x);
            x++;
        }
        System.out.println("method ended");
    }
}
