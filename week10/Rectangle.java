package week10;

public class Rectangle extends Shape{
    int length;
    private int width;

    public Rectangle(int length, int width) {
        //super(); invisible call
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    protected double area(){
        return length * width;
    }

    public double perimeter(){
        return 2 * (length + width);
    }
}
