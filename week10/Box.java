package week10;

public class Box extends Rectangle{

    private int height;
    //String length;  hiding

    public Box(int length, int width, int height) {
        super(length, width);// callling super class constructor
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double area(){
        return 2* super.area() + 2 * height* length + 2 * height * getWidth();
    }

    public double volume(){
        return height * super.area();
    }
}
