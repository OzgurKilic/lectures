package week10;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Proffessor extends Person{


    private ArrayList<Paper> papers;


    public double citationCount(){
        int count = 0;
        for(Paper paper : papers){
            count += paper.getCitationCount();
        }
        return count;
    }



    public ArrayList<Paper> getPapers() {
        return papers;
    }

    public void setPapers(ArrayList<Paper> papers) {
        this.papers = papers;
    }
}
