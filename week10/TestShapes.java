package week10;

public class TestShapes {

    public static void main(String[] args){

        Circle c = new Circle();
        c.setColor("Green");

        Rectangle r  =new Rectangle(4,4);
        r.setColor("Blue");
        r.length = 6;
        System.out.println(r.area());

        Square s = new Square(4,4);
        s.getColor();  //comes from shape
        s.getWidth();  // comes from rectangle


        Box b = new Box(8,5,4);  //area 2* 8 * 5 + 2 * 4* 5 + 2 * 8* 4
        b.setLength(8); // inherited from rectangle
        b.setWidth(5); // inherited from rectangle
        b.setColor("Blue"); // inherited from shape
        b.setHeight(4);  //defined box

        System.out.println("Box Area  = " + b.area());
    }
}
