package week12.drawing.shapes;

public class Rectangle extends Shape{
	

	double width;
	double height;

	
	public Rectangle(double width, double height) {
		super();
		this.width = width;
		this.height = height;
	}

	public double area(){
		return height * width;

	}

	@Override
	public void draw() {
		System.out.println("drawing Rectangle");
	}
}
