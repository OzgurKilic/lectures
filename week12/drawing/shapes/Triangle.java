package week12.drawing.shapes;

public class Triangle extends Shape{
    int side;
    int height;

    public Triangle(int side, int height) {
        this.side = side;
        this.height = height;
    }


    public double area() {
        return (side * height) /2;
    }

    @Override
    public void draw() {
        System.out.println("drawing Triangle");
    }
}
