package week12.drawing.shapes;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Text  implements Drawable {

    String str;

    public Text(String str) {
        this.str = str;
    }

    public void draw() {
        System.out.println("drawing Text " + str);
    }

}
