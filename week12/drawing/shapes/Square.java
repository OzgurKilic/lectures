package week12.drawing.shapes;

public class Square extends Shape{
    private int side;

    public Square(int side) {
        this.side = side;
    }

    public double area(){
        return side * side;

    }

    @Override
    public void draw() {
        System.out.println("drawing Square");
    }
}
