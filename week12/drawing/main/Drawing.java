package week12.drawing.main;


import week12.drawing.shapes.Drawable;
import week12.drawing.shapes.Shape;
import week12.drawing.shapes.Text;

import java.util.ArrayList;

public class Drawing {
	
	private ArrayList<Drawable> drawables = new ArrayList<>();


	public double calculateTotalArea(){
		double totalArea = 0;

		for (Drawable dr : drawables){
			if (dr instanceof Shape)
			totalArea +=((Shape)dr).area();
		}
		return totalArea;
	}
	
	public void addDrawable(Drawable d) {
		drawables.add(d);
	}

	public void draw(){
		for (Drawable d : drawables){
			d.draw();
		}

	}
}
