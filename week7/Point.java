package week7;

public class Point {

    private int x;
    private int y;

    private String label;

    static int count = 0; // default 0

    public Point(){

        count++;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        count++;
    }

    public Point(int x, int y, String name) {
        this.x = x;
        this.y = y;
        label = name;
        count++;
    }


    public void move(int distX, int distY){
        x += distX;
        y += distY;
    }

    public static void mov(Point p, int distX, int distY){
        p.x += distX;
        p.y += distY;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
