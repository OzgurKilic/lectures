package week7;

public class ConstructorDemo {

    public static void main(String args[]){
        Point p = null;
        //p.x = 3; //nullpointerexception

        Point p1 = new Point();
        //p1.label.equals("hello"); nullpointerexception
        if (p1.getLabel() == null){
            System.out.println("label of p1 is null");

        }

        System.out.println(p1.getX() + " " + p1.getY() + " "  + p1.getLabel());
        Point p2 = new Point(3,5);

        Point p3 = new Point(2,3,"hello");
        System.out.println(p3.getLabel());
    }

}
