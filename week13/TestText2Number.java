package week13;

import java.util.Scanner;

public class TestText2Number {

    public static void main(String[] args){
        Text2Number t2n = new Text2Number();
        boolean validInput = true;
        do {
            Scanner reader = new Scanner(System.in);
            System.out.print("Enter a number in text format between (-99 and +99) : ");
            String text = reader.nextLine();

            try {
                int number = t2n.convert(text);
                System.out.println(number);
                validInput = true;
            } catch (InvalidNumberException ex) {
                System.out.println("Invalid Number");
                System.out.println(ex.getMessage());
                validInput = false;
            }

        }while (!validInput);
        System.out.println("program ended");

    }
}
