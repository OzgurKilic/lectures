package week11;

public abstract class Shape {

    protected String color;

    public Shape(){
        System.out.println(" Shape constructor executed");
    }

    public Shape(String color){
        this.color = color;
        System.out.println(" Shape constructor with color executed");
    }
    public abstract double area();

    public String getColor() {
        return color;
    }
}
