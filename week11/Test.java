package week11;

import java.util.ArrayList;
import java.util.List;

public class Test {


    public static void main(String[] args){

        Square s = new Square(10);
        System.out.println(s.area());

        System.out.println(s.getColor());

        //System.out.println( "String rep of square\n" + s );

        Circle c =new Circle(3, "Green");
        System.out.println(c.area());
        //Shape shape = new Shape();  //not meaningful
        System.out.println(c.getColor());
        ArrayList<Shape> shapes = new ArrayList<>();

        shapes.add(s);

        shapes.add(c);

        double total = 0;
        for(Shape shape : shapes){
            total += shape.area();
        }
        System.out.println(total);

    }
}
