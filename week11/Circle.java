package week11;

public class Circle extends Shape{

    private double radius;

    public Circle(double radius) {
        //super();  //invisible
        this.radius = radius;
    }

    public Circle(double radius, String color) {
        super(color);
        this.radius = radius;
    }

    public double area(){
        return Math.PI * radius* radius;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public double getRadius() {
        return radius;
    }
}
