package week11;

public class TestCasting {

    public static void main(String[] args){
        Square s = new Square(5);

        Object obj =s;  //upcasting

        if (s == obj){
            System.out.println("same objects");
        }

        s.area();

        obj = "String";

        ///obj.area();  error accesible are those defined in the variable's class + its super classes
        // Square s2 = (Square) obj //down casting
        // s2.area();

        if (obj instanceof Square)
            ((Square)obj).area();    //down casting
        else{
            System.out.println("obj is not a square");
        }

    }
}
