package week11;

public class Square extends Shape{

    private double side;

    public Square(double side) {
        this.side = side;
    }

    public double area(){
        return  side * side;
    }


    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        for (int i= 0; i < side; i++){
            for (int j= 0; j < side; j++){
                buf.append("S");
            }
            buf.append("\n");
        }
        return buf.toString();
    }
}
