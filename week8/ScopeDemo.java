package week8;

public class ScopeDemo {

    int var1;

    static String var4;


    public static void main(String args[]){

        //var3 = "4"; // instance variables are not accessible from static method
        //scopeMethod(5);  // Object is needed

        ScopeDemo demo = new ScopeDemo();
        demo.var1 = 4;
        ScopeDemo demo2 = new ScopeDemo();
        demo2.var1 = 5;

        ScopeDemo.var4 = "6";

        demo.scopeMethod(6);

    }

    public void scopeMethod(int var1){
        this.var1 = 4;
        var1 = 3;
        var4 = "7";
        String var2;
        if(var1 > 0){
            var2 = "above 0";
        }else{
            var2  = "less than or equal zero";
        }
        System.out.println(var2);
    }

    public void doSomething(){
        //var3 = "hello";
        //var2 = "error"; wrong
        //var1 = 4;   wrong
    }

}
