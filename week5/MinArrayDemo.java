package week5;

public class MinArrayDemo {

    public static void main(String[] args) {

        int[][] numbers = {{ 2,7,1,5}, { 1,-5}, {-3,4,1,3}};
        System.out.println(findMin(numbers));

    }

    public static int findMin(int[][] values){
        int min = values[0][0];

        for (int i = 0; i < values.length; i++){
            for (int j = 0; j < values[i].length; j++) {
                if (min > values[i][j])
                    min = values[i][j];
            }

        }

        return min;

    }
}
