package week5;

public class ArrayInitializeDemo {

    public static void main(String[] args) {

        for (int i = 0; i <args.length ; i++) {
            System.out.println("args[" + i + "] = " + args[i]);
        }

        int[] values = {5,6,3,1}; ///new int[5];

        System.out.println(values);

        for(int i =0; i < values.length; i++){

            System.out.println("values[" + i + "] = " + values[i]);
        }

        System.out.println(sum(values));
    }

    public static int sum(int[] numbers){
        int sum = 0;
        for(int i =0; i < numbers.length; i++){

            sum = sum + numbers[i];
        }
        return sum;
    }
}
