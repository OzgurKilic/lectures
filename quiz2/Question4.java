package quiz2;

public class Question4 {

    public static void main(String[] args) {
        int[][] arr = {{3,4,5},{1,6}};
        System.out.println(findMin(arr));

    }


    public static int findMin(int[][] values){
        int min = values[0][0];
        for (int i=0; i< values.length; i++){
            for (int j=0; j < values[i].length; j++){
                if (min > values[i][j]){
                    min = values[i][j];
                }
            }
        }
        return min;
    }
}
